/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.networkninjas.hash.app;

import cz.networkninjas.hash.app.servlet.HashComparerServlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author Daniel
 */

@WebListener
public class AppConfig implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext ctx = sce.getServletContext();

        // servlet registration, though should not be needed because servlet is annotated and compiled to classes directory
        //ServletRegistration hashComparerServletRegistration = ctx.addServlet("HashComparerServlet", HashComparerServlet.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

}

