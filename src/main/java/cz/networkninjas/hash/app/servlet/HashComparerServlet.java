package cz.networkninjas.hash.app.servlet;

import cz.networkninjas.hashcomputerlib.HashComputer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Daniel
 */
@MultipartConfig(maxFileSize = 1024 * 1024 * 800) //800MB
@WebServlet(name = "HashComparerServlet", urlPatterns = {"/hash", "/"})
public class HashComparerServlet extends HttpServlet {

    
    /**
     * Reserved strings: %md5%,%md5_result%,%sha256%,%sha256_result%
     */
    private final static String WELCOME
            = "<body style=\""
            + "background-image:url('/hash/images/hand_reaching.jpg'); "
            + "background-repeat: no-repeat; "
            + "background-color:#000000; "
            + "background-position:center top;"
            + "color:#ffffff;"
            + "font-family: Arial, Helvetica, sans-serif;"
            + "margin:0 auto;"
            + "width:800px;"
            + "padding:20px;"
            + "\">"
            + "Hey man!"
            + "<h1>Upload file for hash calculations</h1>";
            

    private final static String FORM
            = "<form method=\"post\" enctype=\"multipart/form-data\">\n <br/>"
            + "<label for=\"file\">File <sub>(max 800MB)</sub></label>\n"
            + "<input type=\"file\" name=\"file\" id=\"file\" alt=\"source file for hash calculation\"/>\n <br/>"
            + "<label for=\"your_hash\">Your hash:</label>\n"
            + "<input type=\"text\" id=\"your_hash\" name=\"your_hash\" alt=\"Provide your hash here\"/>\n <br/>"
            /*+ "<select name=\"hash_list\">"
            + "<option value=\"md5\"> md5 </option>"
            + "<option value=\"sha-256\"> sha-256 </option>"
            + "</select>"*/
            + "<input type=\"submit\" value=\"submit\"> \n <br/>"
            + "</form>\n "
            + "<br/>";

    private final static String HASH_HTML
            = "<h2> Results </h2>"
            + "Your hash: %your_hash% <br/>"
            + "SHA256: %sha256% %sha256_result%<br/>"
            + "MD5: %md5% %md5_result%<br/>";

    private final static String OK_RESPONSE
            = "<font color=\"green\"> <b> OK </b> </font>";
    private final static String NO_RESPONSE
            = "";
    private static final String ERROR
            = "Something went wrong. Try again.";

    private static final String HOMEPAGE
            = "<a href=\"http://networkninjas.cz\" alt=\"networkninjas homepage\" style=\"color:#eeeeee\"> networkninjas.cz </a>";

    @Override

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        resp.setContentType("text/html");
        writer.println(WELCOME);
        writer.println(FORM);
        writer.println(HOMEPAGE);
        writer.println("</body>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            PrintWriter writer = resp.getWriter();
            // response assembly
            resp.setContentType("text/html");
            
            //get file from request
            Part part = req.getPart("file");
            
            // no file uploaded
            if (part.getSize() == 0) {

                writer.println("no file uploaded <br/>");
                writer.println(WELCOME);
                writer.println(FORM);
                return;
            }
            System.out.println("Size=" + Long.toString(part.getSize()));
            //filename
            String filename = String.valueOf(System.currentTimeMillis());
            //directory for temporary files7
            File uploads = new File(System.getProperty("java.io.tmpdir"));
            System.out.println("uploads=" + uploads);
            //file with path
            File file = new File(uploads, filename);
            //write to filepath, rewrite existing
            InputStream input = part.getInputStream();
            Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);

            // calculate hashes
            HashComputer hc = new HashComputer();
            hc.setFile(file);
            byte[] md5 = hc.computeHash("MD5");
            byte[] sha256 = hc.computeHash("SHA-256");

            
            

            String result = "";
            result = HASH_HTML.replace("%md5%", HashComputer.toHex(md5));
            result = result.replace("%sha256%", HashComputer.toHex(sha256));
            String yourHash = req.getParameter("your_hash");
            yourHash = yourHash.toUpperCase().trim();
            System.out.println("your_hash=" + yourHash);

            if (!yourHash.isEmpty()) {
                // providedHash
                result = result.replace("%your_hash%", yourHash);

                // MD5
                if (HashComputer.toHex(md5).equals(yourHash)) {
                    result = result.replace("%md5_result%", OK_RESPONSE);
                }

                // SHA256
                if (HashComputer.toHex(sha256).equals(yourHash)) {
                    result = result.replace("%sha256_result%", OK_RESPONSE);
                    System.out.println("YES");
                }
            }

            // clean template
            result = result.replaceAll("%md5_result%|%sha256_result%|%your_hash%", NO_RESPONSE);

            writer.println(WELCOME);
            writer.println(FORM);
            writer.println(result);
            writer.println(HOMEPAGE);
            writer.println("</body>");

            // delete files
            file.delete();
        } catch (FileNotFoundException | NoSuchAlgorithmException | HashComputer.NoAlgorithmSettedException ex) {
            Logger.getLogger(HashComparerServlet.class.getName()).log(Level.SEVERE, null, ex);
            resp.setContentType("text/html");
            resp.getWriter().write(ERROR + "<br/>" + WELCOME + "<br/>" + FORM);
            //resp.getWriter().write(ex.getMessage());
        } catch (IllegalStateException ex) {
            //resp.getWriter().write(ex.getMessage());
            resp.setContentType("text/html");
            resp.getWriter().write(ERROR + "<br/>" + WELCOME + "<br/>" + FORM);
        }

    }

}
