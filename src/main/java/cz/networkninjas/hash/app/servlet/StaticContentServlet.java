package cz.networkninjas.hash.app.servlet;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Daniel
 */
@WebServlet(name = "StaticContentServlet", urlPatterns = {"*.jpg", "*.png"})
public class StaticContentServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        URL aURL = new URL(req.getRequestURL().toString());
        
        /*
        System.out.println("protocol = " + aURL.getProtocol());
        System.out.println("authority = " + aURL.getAuthority());
        System.out.println("host = " + aURL.getHost());
        System.out.println("port = " + aURL.getPort());
        System.out.println("path = " + aURL.getPath());
        System.out.println("query = " + aURL.getQuery());
        System.out.println("filename = " + aURL.getFile());
        System.out.println("ref = " + aURL.getRef());
         */

        String path = aURL.getPath();
        
        int k = path.lastIndexOf("/");
        if ((k == -1)) {resp.getWriter().write("Resource not found"); return;}
            
        
        String filename = path.substring(k);
        
        InputStream in = getServletContext().getResourceAsStream("/images/"+filename);
        OutputStream out = resp.getOutputStream();
        byte [] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = in.read(buffer)) != -1){
            out.write(buffer,0,bytesRead);
        }
        

    }

}
