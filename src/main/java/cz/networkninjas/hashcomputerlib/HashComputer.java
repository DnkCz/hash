package cz.networkninjas.hashcomputerlib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Daniel
 */
public class HashComputer {

    /**
     * File to be read of.
     */
    private File file;

    /**
     * Algorithm used to calculate message digest.
     */
    private String algorithm;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getAlgorithm() {
        return this.algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * Computes hash for given file and message digest.
     *
     * @param algorithm
     * @return byte array of computed hash
     * @throws java.io.FileNotFoundException
     * @throws java.security.NoSuchAlgorithmException
     * @throws
     * cz.networkninjas.hashcomputerlib.HashComputer.NoAlgorithmSettedException
     */
    public byte[] computeHash(String algorithm) throws FileNotFoundException, NoSuchAlgorithmException, IOException, NoAlgorithmSettedException {
        this.setAlgorithm(algorithm);
        return computeHash();
    }

    /**
     * Computes hash for given file and message digest.
     *
     * @return byte array of computed hash
     * @throws java.io.FileNotFoundException
     * @throws java.security.NoSuchAlgorithmException
     * @throws
     * cz.networkninjas.hashcomputerlib.HashComputer.NoAlgorithmSettedException
     */
    public byte[] computeHash() throws FileNotFoundException, NoSuchAlgorithmException, IOException, NoAlgorithmSettedException {
        if (getAlgorithm() == null || getAlgorithm().isEmpty()) {
            throw new NoAlgorithmSettedException();
        }
        FileInputStream fileInputStream = new FileInputStream(getFile());
        MessageDigest messageDigest = MessageDigest.getInstance(getAlgorithm());
        byte[] buffer = new byte[1024 * 64];
        int numOfBytes;
        while ((numOfBytes = fileInputStream.read(buffer)) > 0) {
            messageDigest.update(buffer, 0, numOfBytes);
        }
        return messageDigest.digest();
    }

    /**
     * Converts an array of bytes into a string.
     *
     * @param bytes
     * @return converted
     */
    public static String toHex(byte[] bytes) {
        return DatatypeConverter.printHexBinary(bytes);
    }

    /**
     * Exception thrown when no algorithm was set.
     */
    public class NoAlgorithmSettedException extends Exception {

        // algorithm needs to be setted first
        public NoAlgorithmSettedException() {
            super("No algorithm was setted.");
        }
    }

}
